package com.t3infotech.societyapp.data.repository

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.t3infotech.societyapp.data.network.NetworkSource

class InteractorViewModelFactory(private val repo: NetworkSource): ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return modelClass.getConstructor(NetworkSource::class.java).newInstance(repo)
    }
}