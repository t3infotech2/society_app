package com.t3infotech.societyapp.data.network

import android.net.Uri
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object ApiClient {


    // Create a new object from HttpLoggingInterceptor
    private var interceptor = HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY)

    // Add Interceptor to HttpClient
    private val client = OkHttpClient.Builder().addInterceptor(interceptor).build()

    private const val BASE_URL: String = "societyapi.t3infotech.com"

    private val url = Uri.Builder()
        .scheme("https")
        .authority(BASE_URL).toString();

    var retrofit: Retrofit =
        Retrofit.Builder().baseUrl(url)
            .client(client)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
}