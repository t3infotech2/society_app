package com.t3infotech.societyapp.data.network

import com.t3infotech.societyapp.data.model.CommonResponse
import com.t3infotech.societyapp.data.model.LoginModel
import com.t3infotech.societyapp.data.model.RegisterModel
import com.t3infotech.societyapp.data.model.UserModel
import retrofit2.Call
import retrofit2.http.*

interface ApiClientInterface {

    @FormUrlEncoded
    @POST("/login.php")
    fun loginUser(@Field("phone_number") phone_no: Int, @Field("password") password: String): Call<LoginModel>

    @FormUrlEncoded
    @POST("/register.php")
    fun registerUser(@Field("phone_number") phone_no: Int, @FieldMap map: HashMap<String, String?>): Call<RegisterModel>

    @GET("/all_users.php")
    fun fetchAllUsers(): Call<UserModel>

    @GET("/pending_user.php")
    fun fetchPendingUser(): Call<UserModel>

    // Pending api's
    @FormUrlEncoded
    @POST("/block_user.php")
    fun blockUser(@Field("id") id: Int): Call<CommonResponse>





}