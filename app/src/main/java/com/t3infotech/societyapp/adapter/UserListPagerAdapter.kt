package com.t3infotech.societyapp.adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import com.t3infotech.societyapp.ui.AllUserListFragment
import com.t3infotech.societyapp.ui.PendingUserListFragment

class UserListPagerAdapter(fragmentManager: FragmentManager, private val tabs: Array<String>) :
    FragmentStatePagerAdapter(fragmentManager, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {


    override fun getCount(): Int {
        return tabs.size
    }

    override fun getPageTitle(position: Int): CharSequence? {
        var title: String
        tabs.let { tabs ->
            tabs[position].let { name ->
                title = name
            }
        }
        return title
    }

    override fun getItem(position: Int): Fragment {
        return when (position) {
            0 -> AllUserListFragment.getInstance()
            else -> PendingUserListFragment.getInstance()
        }
    }


}