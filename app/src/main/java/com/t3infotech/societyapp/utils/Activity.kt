package com.t3infotech.societyapp.utils

import android.app.Activity
import android.view.inputmethod.InputMethodManager
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction


fun AppCompatActivity.replaceFragment(layoutId: Int, fragment: Fragment): FragmentTransaction {
    return this.supportFragmentManager.beginTransaction()
        .replace(layoutId, fragment)
}

fun AppCompatActivity.addFragment(layoutId: Int, fragment: Fragment, addToBackStack: Boolean = false): FragmentTransaction {
    return if (addToBackStack) {
        supportFragmentManager.beginTransaction()
            .add(layoutId, fragment).addToBackStack(null)
    } else {
        supportFragmentManager.beginTransaction()
            .add(layoutId, fragment)
    }
}

fun Activity.hideSoftKeyboard() {
    val inputMethodManager = this.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
    if (inputMethodManager.isActive) {
        if (this.currentFocus != null) {
            this.currentFocus?.run {
                inputMethodManager.hideSoftInputFromWindow(this.windowToken, 0)
            }
        }
    }
}