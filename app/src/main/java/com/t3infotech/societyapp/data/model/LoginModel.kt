package com.t3infotech.societyapp.data.model

import com.google.gson.annotations.SerializedName

data class LoginModel(
    @SerializedName("message") val message: String? = null,
    @SerializedName("status") val status: Boolean = false,
    @SerializedName("is_admin") val isAdmin: Int = -1,
    @SerializedName("is_accepted") val isAccepted: Int = -1
)