package com.t3infotech.societyapp.data.model

import com.google.gson.annotations.SerializedName

data class ResponseModel(
    @SerializedName("id") val id: Int = 0,
    @SerializedName("f_name") val fName: String = "",
    @SerializedName("l_name") val lName: String = "",
    @SerializedName("email_id") val emailId: String = "",
    @SerializedName("phone_number") val phoneNumber: Int = 0,
    @SerializedName("is_accepted") val isAccepted: Int = 0,
    @SerializedName("is_admin") val isAdmin: Int = 0
)

