package com.t3infotech.societyapp.ui

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.t3infotech.societyapp.adapter.RecyclerAdapterCallback
import com.t3infotech.societyapp.adapter.UserRecyclerAdapter
import com.t3infotech.societyapp.data.network.ApiClient
import com.t3infotech.societyapp.data.network.NetworkSource
import com.t3infotech.societyapp.data.repository.Interactor
import com.t3infotech.societyapp.data.repository.InteractorViewModelFactory
import com.t3infotech.societyapp.databinding.FragmentUserListRecyclerBinding

class PendingUserListFragment : Fragment(), RecyclerAdapterCallback {

    private lateinit var interactor: Interactor
    private lateinit var binding: FragmentUserListRecyclerBinding
    private lateinit var userRecyclerAdapter: UserRecyclerAdapter

    override fun onAttach(context: Context) {
        super.onAttach(context)
        val interactorViewModelFactory = InteractorViewModelFactory(NetworkSource(context, ApiClient.retrofit))
        activity?.run {
            interactor = ViewModelProvider(this, interactorViewModelFactory).get(Interactor::class.java)
        }
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentUserListRecyclerBinding.inflate(inflater, container, false)

        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        binding.recycler.setHasFixedSize(true);

        // use a linear layout manager
        val layoutManager = LinearLayoutManager(requireContext());
        binding.recycler.layoutManager = layoutManager
        loadData()
        return binding.root
    }

    private fun loadData() {
        interactor.fetchPendingUser().observe(viewLifecycleOwner, Observer {
            if (it.status) {
                userRecyclerAdapter = UserRecyclerAdapter(it.response, this)
                binding.recycler.adapter = userRecyclerAdapter
            } else {
            }
        })
    }

    companion object {

        @JvmStatic
        fun getInstance(): PendingUserListFragment {
            return PendingUserListFragment()
        }
    }

    override fun acceptUser(id: Int, position: Int) {


    }

    override fun deleteUser(id: Int, position: Int){
    }

    override fun blockUser(id: Int, position: Int) {
        interactor.blockUser(id).observe(viewLifecycleOwner, Observer {
            if (it.status) {
                userRecyclerAdapter.removeItemAt(position)
            } else {
                Log.d("###", "something went wrong")
            }
        })
    }
}