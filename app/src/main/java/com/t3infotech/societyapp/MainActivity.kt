package com.t3infotech.societyapp

import android.os.Bundle
import android.os.PersistableBundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.t3infotech.societyapp.ui.LoginFragment
import com.t3infotech.societyapp.utils.PreferenceUtils
import com.t3infotech.societyapp.utils.addFragment

class MainActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}