package com.t3infotech.societyapp.data.repository

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.t3infotech.societyapp.data.model.CommonResponse
import com.t3infotech.societyapp.data.model.LoginModel
import com.t3infotech.societyapp.data.model.RegisterModel
import com.t3infotech.societyapp.data.model.UserModel
import com.t3infotech.societyapp.data.network.NetworkSource

class Interactor(private val repo: NetworkSource): ViewModel() {

    fun loginUser(phone_no: Int, password: String): MutableLiveData<LoginModel> {
        return repo.loginUser(phone_no, password)
    }

    fun registerUser(phone_no: Int, map: HashMap<String, String?>): MutableLiveData<RegisterModel> {
        return repo.registerUser(phone_no, map)
    }

    fun fetchAllUser(): MutableLiveData<UserModel> {
        return repo.fetchAllUsers()
    }

    fun fetchPendingUser(): MutableLiveData<UserModel> {
        return repo.fetchPendingUser()
    }

    fun blockUser(id: Int): MutableLiveData<CommonResponse> {
        return repo.blockUser(id)
    }
}