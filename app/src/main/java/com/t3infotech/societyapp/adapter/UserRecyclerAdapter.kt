package com.t3infotech.societyapp.adapter

import android.util.Log
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.appcompat.widget.PopupMenu
import androidx.recyclerview.widget.RecyclerView
import com.t3infotech.societyapp.R
import com.t3infotech.societyapp.data.model.ResponseModel


class UserRecyclerAdapter(private val myDataset: MutableList<ResponseModel>, private val callback: RecyclerAdapterCallback) :
    RecyclerView.Adapter<UserRecyclerAdapter.MyViewHolder>() {

    fun removeItemAt(position : Int) {
        myDataset.removeAt(position)
        this.notifyItemRemoved(position)
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): UserRecyclerAdapter.MyViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.user_list_item, parent, false)

        return MyViewHolder(view)
    }

    override fun getItemCount(): Int {
        return myDataset.size
    }

    override fun onBindViewHolder(holder: UserRecyclerAdapter.MyViewHolder, position: Int) {
        val nameTxt = holder.view.findViewById<AppCompatTextView>(R.id.user_name)
        val phoneTxt = holder.view.findViewById<AppCompatTextView>(R.id.user_phone_no)
        val btnMenu = holder.view.findViewById<AppCompatImageView>(R.id.btn_menu)
        nameTxt.text = "${myDataset[position].fName} ${myDataset[position].lName}"
        phoneTxt.text = myDataset[position].phoneNumber.toString()


        btnMenu.setOnClickListener {
            val popup = PopupMenu(btnMenu.context, btnMenu)
            //inflating menu from xml resource
            popup.inflate(R.menu.list_menu)
            //adding click listener
            popup.setOnMenuItemClickListener { item ->
                when (item?.itemId) {
                    R.id.accept -> {
                        callback.acceptUser(myDataset[position].id, position)
                        true
                    }
                    R.id.block -> {
                       callback.blockUser(myDataset[position].id, position)
                        true
                    }
                    R.id.delete -> {
                        callback.deleteUser(myDataset[position].id, position)
                        true
                    }
                    else -> {
                        false
                    }
                }
            }
            //displaying the popup
            popup.show()
        }

    }

    class MyViewHolder(val view: View) : RecyclerView.ViewHolder(view)

}