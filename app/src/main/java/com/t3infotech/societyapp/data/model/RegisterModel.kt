package com.t3infotech.societyapp.data.model

import com.google.gson.annotations.SerializedName

data class RegisterModel(
    @SerializedName("message") val message: String? = null,
    @SerializedName("status") val status: Boolean = false,
    @SerializedName("phone_no") val phoneNo: Int = 0,
    @SerializedName("is_admin") val isAdmin: Int = -1,
    @SerializedName("is_accepted") val isAccepted: Int = -1
)