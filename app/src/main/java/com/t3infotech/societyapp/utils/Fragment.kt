package com.t3infotech.societyapp.utils

import android.app.Activity
import android.view.inputmethod.InputMethodManager
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction

fun Fragment.replaceFragment(layoutId: Int, fragment: Fragment): FragmentTransaction? {
    return this.activity?.supportFragmentManager?.beginTransaction()
        ?.replace(layoutId, fragment)
}

fun Fragment.addFragment(layoutId: Int, fragment: Fragment, addToBackStack: Boolean = false, tag: String? = null): FragmentTransaction? {
    return if (addToBackStack) {
        childFragmentManager.beginTransaction().add(layoutId, fragment, tag).addToBackStack(null)
    } else {
        childFragmentManager.beginTransaction().add(layoutId, fragment, tag)
    }
}

fun Fragment.findFragment(tag: String): Fragment? {
    return childFragmentManager.findFragmentByTag(tag)
}

fun Fragment.hideSoftKeyboard() {
    this.activity?.run {
        val inputMethodManager = getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        if (inputMethodManager.isActive) {
            if (currentFocus != null) {
                currentFocus?.run {
                    inputMethodManager.hideSoftInputFromWindow(this.windowToken, 0)
                }
            }
        }
    }
}
