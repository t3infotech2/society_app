package com.t3infotech.societyapp.ui

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.Navigation

import com.t3infotech.societyapp.adapter.UserListPagerAdapter
import com.t3infotech.societyapp.data.network.ApiClient
import com.t3infotech.societyapp.data.network.NetworkSource
import com.t3infotech.societyapp.data.repository.Interactor
import com.t3infotech.societyapp.data.repository.InteractorViewModelFactory
import com.t3infotech.societyapp.databinding.FragmentUserListParentBinding

/**
 * A simple [Fragment] subclass.
 */
class UserListParentFragment : Fragment() {

    private lateinit var interactor: Interactor
    private lateinit var binding: FragmentUserListParentBinding

    lateinit var navController: NavController

    override fun onAttach(context: Context) {
        super.onAttach(context)
        val interactorViewModelFactory = InteractorViewModelFactory(NetworkSource(context, ApiClient.retrofit))
        activity?.run {
            interactor = ViewModelProvider(this, interactorViewModelFactory).get(Interactor::class.java)
        }    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentUserListParentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        navController = Navigation.findNavController(view)
//        navController.popBackStack(R.id.action_loginFragment_to_userListParentFragment, true)
//        navController.popBackStack(R.id.action_registerFragment_to_userListParentFragment, true)


        val tabArray = arrayOf("All Users", "Pending Users")
        binding.viewPager.adapter = UserListPagerAdapter(childFragmentManager, tabArray)
        binding.tabLayout.setupWithViewPager(binding.viewPager)
    }

}
