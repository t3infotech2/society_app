package com.t3infotech.societyapp.ui

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.Navigation
import com.t3infotech.societyapp.R
import com.t3infotech.societyapp.data.network.ApiClient
import com.t3infotech.societyapp.data.network.NetworkSource
import com.t3infotech.societyapp.data.repository.Interactor
import com.t3infotech.societyapp.data.repository.InteractorViewModelFactory
import com.t3infotech.societyapp.databinding.FragmentRegisterBinding
import com.t3infotech.societyapp.utils.PreferenceUtils

class RegisterFragment : Fragment() {

    private lateinit var interactor: Interactor
    private lateinit var binding: FragmentRegisterBinding

    lateinit var navController: NavController

    lateinit var phoneNo: String
    lateinit var password: String
    lateinit var confirmPassword: String

    override fun onAttach(context: Context) {
        super.onAttach(context)
        val interactorViewModelFactory = InteractorViewModelFactory(NetworkSource(context, ApiClient.retrofit))
        activity?.run {
            interactor = ViewModelProvider(this, interactorViewModelFactory).get(Interactor::class.java)
        }    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentRegisterBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        navController = Navigation.findNavController(view)
        initializeListeners()
    }


    private fun initializeListeners() {
        binding.btnRegister.setOnClickListener {

            val map = HashMap<String, String?>()
            map["f_name"] = binding.firstName.text.toString()
            map["l_name"] = binding.lastName.text.toString()
            phoneNo = binding.phoneNumber.text.toString()
            map["email_id"] = binding.email.text.toString()
            password = binding.txtPassword.text.toString()
            confirmPassword = binding.txtConfirmPassword.text.toString()

            if (password != confirmPassword) {
                Log.d("###", "password not match")
            } else {
                map["password"] = password
                interactor.registerUser(phoneNo.toInt(), map)
                    .observe(viewLifecycleOwner, Observer {

                        if (it.status) {
                            PreferenceUtils.setUserLoginStatus(requireContext(), true)
                            if (it.isAdmin == 1) {
                                PreferenceUtils.setIsUserAdmin(requireContext(), true)
                                navController.navigate(R.id.action_registerFragment_to_userListParentFragment)
                            } else if (it.isAccepted == 1) {
                                PreferenceUtils.setIsUserAdmin(requireContext(), false)
                                PreferenceUtils.setIsUserApproved(requireContext(), true)
                                navController.navigate(R.id.action_registerFragment_to_userHomeFragment)
                            } else {
                                PreferenceUtils.setIsUserAdmin(requireContext(), false)
                                PreferenceUtils.setIsUserApproved(requireContext(), false)
                                navController.navigate(R.id.action_registerFragment_to_approvalPendingFragment)
                            }
                        } else {
                            Log.d("###", "user not registered! - ${it.message}")
                        }
                    })
            }
        }

        binding.signIn.setOnClickListener {
            navController.navigate(R.id.action_registerFragment_to_loginFragment)

        }
    }

    companion object {
        @JvmStatic
        fun getInstance(): RegisterFragment {
            return RegisterFragment()
        }
    }
}