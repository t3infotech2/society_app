package com.t3infotech.societyapp

object Constants{
    const val SHARED_PREF_APP = "shared_pref_app"
    const val IS_USER_LOGGED_IN = "prefs_is_user_logged_in"
    const val USER_NAME = "user_name"
    const val USER_PHONE_NUMBER = "user_phone_number"
    const val IS_ADMIN = "is_admin"
    const val IS_APPROVED = "is_approved"
}