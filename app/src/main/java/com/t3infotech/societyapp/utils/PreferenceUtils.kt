package com.t3infotech.societyapp.utils

import android.content.Context
import android.content.SharedPreferences
import com.t3infotech.societyapp.Constants.IS_ADMIN
import com.t3infotech.societyapp.Constants.IS_APPROVED
import com.t3infotech.societyapp.Constants.IS_USER_LOGGED_IN
import com.t3infotech.societyapp.Constants.SHARED_PREF_APP
import com.t3infotech.societyapp.Constants.USER_NAME
import com.t3infotech.societyapp.Constants.USER_PHONE_NUMBER

object PreferenceUtils {

    private lateinit var sharedPreferences: SharedPreferences

    @JvmStatic
    private fun editSharedPreference(context: Context): SharedPreferences {
        return context.getSharedPreferences(
            SHARED_PREF_APP,
            Context.MODE_PRIVATE
        )
    }

    fun setUserLoginStatus(context: Context, isLoggedIn: Boolean) {
        PreferenceUtils.editSharedPreference(context).edit().putBoolean(IS_USER_LOGGED_IN, isLoggedIn).apply()
    }

    fun isUserLoggedIn(context: Context): Boolean {
        val prefs: SharedPreferences = PreferenceUtils.editSharedPreference(context)
        return prefs.getBoolean(IS_USER_LOGGED_IN, false)
    }

    fun setUserName(context: Context, userName: String) {
        PreferenceUtils.editSharedPreference(context).edit().putString(USER_NAME, userName).apply()
    }

    fun getUserName(context: Context): String? {
        val prefs: SharedPreferences = PreferenceUtils.editSharedPreference(context)
        return prefs.getString(USER_NAME, "")
    }

    fun setUserPhoneNo(context: Context, userPhoneNo: Int) {
        PreferenceUtils.editSharedPreference(context).edit().putInt(USER_PHONE_NUMBER, userPhoneNo).apply()
    }

    fun getUserPhoneNo(context: Context): Int {
        val prefs: SharedPreferences = PreferenceUtils.editSharedPreference(context)
        return prefs.getInt(USER_PHONE_NUMBER, 0)
    }


    fun setIsUserAdmin(context: Context, isAdmin: Boolean) {
        PreferenceUtils.editSharedPreference(context).edit().putBoolean(IS_ADMIN, isAdmin).apply()
    }

    fun isUserAdmin(context: Context): Boolean {
        val prefs: SharedPreferences = PreferenceUtils.editSharedPreference(context)
        return prefs.getBoolean(IS_ADMIN, false)
    }


    fun setIsUserApproved(context: Context, isAdmin: Boolean) {
        PreferenceUtils.editSharedPreference(context).edit().putBoolean(IS_APPROVED, isAdmin).apply()
    }

    fun isUserApproved(context: Context): Boolean {
        val prefs: SharedPreferences = PreferenceUtils.editSharedPreference(context)
        return prefs.getBoolean(IS_APPROVED, false)
    }

}