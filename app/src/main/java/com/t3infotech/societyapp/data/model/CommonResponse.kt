package com.t3infotech.societyapp.data.model

import com.google.gson.annotations.SerializedName

data class CommonResponse(
    @SerializedName("message") val message: String? = null,
    @SerializedName("status") val status: Boolean = false
)