package com.t3infotech.societyapp.data.model

import com.google.gson.annotations.SerializedName

data class UserModel(
    @SerializedName("message") val message: String? = null,
    @SerializedName("status") val status: Boolean = false,
    @SerializedName("count") val count: Int = 0,
    @SerializedName("response") val response: MutableList<ResponseModel> = mutableListOf()
    )
