package com.t3infotech.societyapp.data.network

import android.content.Context
import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.t3infotech.societyapp.data.model.CommonResponse
import com.t3infotech.societyapp.data.model.LoginModel
import com.t3infotech.societyapp.data.model.RegisterModel
import com.t3infotech.societyapp.data.model.UserModel
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit

class NetworkSource(private val context: Context, private val retrofit: Retrofit) {


    fun loginUser(phoneNo: Int, password: String): MutableLiveData<LoginModel> {
        val loginLiveData: MutableLiveData<LoginModel> = MutableLiveData()
        retrofit.create(ApiClientInterface::class.java).loginUser(phoneNo, password).enqueue(object :Callback<LoginModel> {
            override fun onFailure(call: Call<LoginModel>, t: Throwable) {
                TODO("Failure Handling")
            }

            override fun onResponse(call: Call<LoginModel>, response: Response<LoginModel>) {
                loginLiveData.value = response.body()
            }
        })
        return loginLiveData
    }

    fun registerUser(phoneNo: Int, map: HashMap<String, String?>): MutableLiveData<RegisterModel> {
        val registerLiveData: MutableLiveData<RegisterModel> = MutableLiveData()

        retrofit.create(ApiClientInterface::class.java).registerUser(phoneNo, map).enqueue(object :Callback<RegisterModel> {
            override fun onFailure(call: Call<RegisterModel>, t: Throwable) {
                // Failure Handling
            }

            override fun onResponse(call: Call<RegisterModel>, response: Response<RegisterModel>) {
                registerLiveData.value = response.body()
            }
        })
        return registerLiveData
    }


    fun fetchAllUsers(): MutableLiveData<UserModel> {
        val allUserLiveData: MutableLiveData<UserModel> = MutableLiveData()

        retrofit.create(ApiClientInterface::class.java).fetchAllUsers().enqueue(object :Callback<UserModel> {
            override fun onFailure(call: Call<UserModel>, t: Throwable) {
                // Failure Handling
            }

            override fun onResponse(call: Call<UserModel>, response: Response<UserModel>) {
                allUserLiveData.value = response.body()
            }
        })
        return allUserLiveData
    }

    fun fetchPendingUser(): MutableLiveData<UserModel> {
        val pendingUserLiveData: MutableLiveData<UserModel> = MutableLiveData()

        retrofit.create(ApiClientInterface::class.java).fetchPendingUser()
            .enqueue(object : Callback<UserModel> {
                override fun onFailure(call: Call<UserModel>, t: Throwable) {
                    // Failure Handling
                }

                override fun onResponse(call: Call<UserModel>, response: Response<UserModel>) {
                    pendingUserLiveData.value = response.body()
                }
            })
        return pendingUserLiveData
    }

    fun blockUser(id: Int): MutableLiveData<CommonResponse> {
        val blockUserLiveData: MutableLiveData<CommonResponse> = MutableLiveData()

        retrofit.create(ApiClientInterface::class.java).blockUser(id)
            .enqueue(object : Callback<CommonResponse> {
                override fun onFailure(call: Call<CommonResponse>, t: Throwable) {
                    // Failure Handling
                }

                override fun onResponse(call: Call<CommonResponse>, response: Response<CommonResponse>) {
                    blockUserLiveData.value = response.body()
                }
            })
        return blockUserLiveData
    }

}