package com.t3infotech.societyapp.adapter

interface RecyclerAdapterCallback {

    fun acceptUser(id: Int, position: Int)
    fun deleteUser(id: Int, position: Int)
    fun blockUser(id: Int, position: Int)
}