package com.t3infotech.societyapp.ui

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.NavController
import androidx.navigation.Navigation

import com.t3infotech.societyapp.R
import com.t3infotech.societyapp.utils.PreferenceUtils

/**
 * A simple [Fragment] subclass.
 */
class MainFragment : Fragment() {

    lateinit var navController: NavController

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_main, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        navController = Navigation.findNavController(view)
        val isLoggedIn = PreferenceUtils.isUserLoggedIn(requireContext())
        val isAdmin = PreferenceUtils.isUserAdmin(requireContext())
        val isAccepted = PreferenceUtils.isUserApproved(requireContext())

        if (isLoggedIn) {
            if(isAdmin) {
                navController.navigate(R.id.action_mainFragment_to_userListParentFragment)
            } else if(isAccepted){
                navController.navigate(R.id.action_mainFragment_to_userHomeFragment)
            } else {
                navController.navigate(R.id.action_mainFragment_to_approvalPendingFragment)
            }
        } else {
            navController.navigate(R.id.action_mainFragment_to_loginFragment)
        }
    }
}
